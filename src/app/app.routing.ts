import { NgModule } from '@angular/core';
import { Routes, RouterModule, Router } from '@angular/router';

// Layouts
import { CommonLayoutComponent } from './common/common-layout.component';
import { AuthenticationLayoutComponent } from './common/authentication-layout.component';
import { AuthGuard } from 'src/app/_guards/auth.guard';

export const AppRoutes: Routes = [
    {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full',
    },
    {
        path: 'auth',
        loadChildren: './pages/auth/auth.module#AuthModule',
    },
    {
        path: '',
        canActivate: [AuthGuard],
        component: CommonLayoutComponent,
        children: [
            {
                path: 'dashboard',
                loadChildren: './dashboard/dashboard.module#DashboardModule'
            }
        ]
    }
];

@NgModule({
    imports: [ RouterModule.forRoot(AppRoutes, { useHash: false }) ],
    exports: [ RouterModule ]
})
export class AppRoutingModule {

    constructor(private _router: Router) {
        this._router.errorHandler = (error: any) => {
            this._router.navigate(['/']);
        }
    }
}

