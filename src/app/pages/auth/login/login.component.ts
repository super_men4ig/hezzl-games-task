import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { AuthenticationService } from '../../../_services/core/authentication.service';

import 'rxjs/add/operator/finally';
import { BehaviorSubject } from 'rxjs';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';
import { cmp } from '../../../_helpers/collectionHelper';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
  })
export class LoginComponent {
    private returnUrl: string;

    // * behavior
    public isLoading = false;
    // * behavior

    // * app-login-form
    form: FormGroup;
    email: AbstractControl;
    password: AbstractControl;
    isRememberMe: AbstractControl;
    // * app-login-form

    langs: BehaviorSubject<Array<any>>;

    constructor(private _authService: AuthenticationService,
                private _route: ActivatedRoute,
                private _router: Router,
                public translate: TranslateService,
                fb: FormBuilder)
    {
        console.info('ctr() LoginComponent');
        this.returnUrl = this._route.snapshot.queryParams['returnUrl'] || '/';
        this._authService.logout();

        this.form = fb.group({
            'email': ['', Validators.compose([Validators.required, Validators.minLength(4)])],
            'password': ['', Validators.compose([Validators.required, Validators.minLength(6)])],
            'isRememberMe': [true, Validators.compose([Validators.required])]
        });
    
        this.email = this.form.controls['email'];
        this.password = this.form.controls['password'];
        this.isRememberMe = this.form.controls['isRememberMe'];

        let tLangs = this.translate.getLangs().map(
            (l) => {
                var o = {
                    strength: (l == this.translate.currentLang) ? 0 : 1,
                    name: l
                };
                return o;
            }
        );
      
        this.langs = new BehaviorSubject(tLangs.sort((a, b) => {
            return cmp(a.strength,b.strength) || cmp(a.name, b.name);
        }));
      
        translate.onLangChange.subscribe((event: LangChangeEvent) => {
            this.onChangeLang(event.lang);
        });
    }

    onChangeLang(lang) {
        this.translate.use(lang);
        this.translate.currentLang = lang;
        let tLangs = this.translate.getLangs().map(
            (l) => {
                var o = {
                    strength: (l == this.translate.currentLang) ? 0 : 1,
                    name: l
                };
                return o;
            }
        );
    
        this.langs.next(tLangs.sort((a, b) => {
            return cmp(a.strength,b.strength) || cmp(a.name, b.name);
        }));
      }

    onSubmit(values: any) {
        console.info(values);

        this.isLoading = true;

        // TODO: сделать Interceptor, который будет преобразовывать в formData
        let data = new FormData();

        for (var prop in values) {
            data.append(prop, values[prop]);
        }

        this._authService.login(data, values.isRememberMe).finally(() => { this.isLoading = false; }).subscribe(
            (response) => {
                this._router.navigate([this.returnUrl]);
            },
            (err) => {
                alert(`${err.status} ${err.message}`);
                console.error(err);
            }
        );
    }
}