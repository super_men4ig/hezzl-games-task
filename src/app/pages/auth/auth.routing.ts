import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './login/login.component';
import { ModuleWithProviders } from '@angular/core';
import { RegisterComponent } from './register/register.component';

// noinspection TypeScriptValidateTypes
export const routes = [
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'register',
    component: RegisterComponent
  },
  { path: '**', redirectTo: 'login' }
];

export const AuthModuleRoutes: ModuleWithProviders = RouterModule.forChild(routes);
