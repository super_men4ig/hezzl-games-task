import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/_services/core/authentication.service';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html'
})
export class AppComponent implements OnInit {
    public currentLang: string;
    public currentUser = null;

    constructor(private _authService: AuthenticationService,
                private _router: Router,
                private _activeRoute: ActivatedRoute,
                public translate: TranslateService) 
    {
    
        translate.addLangs(["en", "ru"]);

        if (localStorage.getItem('lang')) {
            translate.use(localStorage.getItem('lang'));
            this.currentLang = localStorage.getItem('lang');
        } else {
            translate.setDefaultLang("ru");
            let browserLang = translate.getBrowserLang();
            translate.use(browserLang.match(/en|ru/) ? browserLang : 'ru');
            this.currentLang = browserLang.match(/en|ru/) ? browserLang : 'ru';
        }

        translate.onLangChange.subscribe((event: LangChangeEvent) => {
            translate.setDefaultLang(event.lang);
            localStorage.setItem('lang', event.lang);
        });

        this._activeRoute.queryParams.subscribe(params => {
            this.currentLang = params['lang'] || this.currentLang;
            translate.use(this.currentLang);
        });

    }

    ngOnInit() {
        this._authService.currentUser$.subscribe((cu: any) => {
            this.currentUser = cu;
            if (cu == null || cu.expiredAt == null) return;

            this._authService.valid().subscribe(
                (response) => {
                    console.info(response);
                },
                (err) => {
                    console.error(err);
                }
            );
            // let expiredAt = new Date(cu.expiredAt);

            // if (expiredAt > new Date()) {
            //     console.info('CALLS');
            //     this._authService.refresh().subscribe(
            //         (response) => {
            //             console.info(response);
            //         },
            //         (err) => {
            //             console.error(err);
            //         }
            //     );
            // } else {
            //     this._authService.valid().subscribe(
            //         (response) => {
            //             console.info(response);
            //         },
            //         (err) => {
            //             console.error(err);
            //         }
            //     );
            // }
        });
    }
}
