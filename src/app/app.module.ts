import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpClientModule, HttpClient, HTTP_INTERCEPTORS } from '@angular/common/http';

//Layout Modules
import { CommonLayoutComponent } from './common/common-layout.component';
import { AuthenticationLayoutComponent } from './common/authentication-layout.component';

//Directives
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { Sidebar_Directives } from './shared/directives/side-nav.directive';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';

// Routing Module
import { AppRoutes, AppRoutingModule } from './app.routing';

// App Component
import { AppComponent } from './app.component';
import { AuthenticationService } from 'src/app/_services/core/authentication.service';
import { AuthGuard } from 'src/app/_guards/auth.guard';
import { AuthInterceptor } from 'src/app/_services/interceptors/AuthInterceptor';

import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { createTranslateLoader } from './_services/core/translateloader';
import { SharedService } from './_services/shared.service';
import { ThemeSelectorComponent } from 'src/app/_components/theme-selector/theme-selector.component';
import { ThemeSelectorService } from 'src/app/_services/ui/theme-selector.service';
import { Cards_Directives } from 'src/app/shared/directives/cards.directive';

@NgModule({
    imports: [
        BrowserModule,
        AppRoutingModule,
        NgbModule.forRoot(),
        HttpClientModule,
        FormsModule,
        PerfectScrollbarModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: createTranslateLoader,
                deps: [HttpClient]
            }
        })
    ],
    declarations: [
        AppComponent,
        CommonLayoutComponent,
        AuthenticationLayoutComponent,
        Sidebar_Directives,
        Cards_Directives,
        ThemeSelectorComponent
    ],
    providers: [
        { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
        AuthenticationService,
        AuthGuard,
        SharedService,
        ThemeSelectorService,
    ],
    bootstrap: [AppComponent]
})


export class AppModule { }
