import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { throwError } from 'rxjs/internal/observable/throwError';

@Injectable()
export class AuthenticationService {
    serviceEndPoint: string = 'admin/';
    apiEndPoint: string;
    token: string;
    currentUser = null;
    currentUser$: BehaviorSubject<Object>;

    constructor(private http: HttpClient) {
        this.currentUser$ = <BehaviorSubject<Object>> new BehaviorSubject(new Object());
        const currentUser = JSON.parse(localStorage.getItem('currentUser'));
        this.currentUser = currentUser;
        this.currentUser$.next(this.currentUser);
        this.token = currentUser && currentUser.token;
        this.apiEndPoint = `${environment.apiEndPoint + this.serviceEndPoint}`;
    }

    getToken() {
        if (this.token == null) {
            this.token = localStorage.getItem('accessToken');
            return this.token;
        } else {
            return this.token;
        }
    }

    login(formData, isRememberMe) {
        return this.http.post(`${this.apiEndPoint}login`, formData)
            .map((response: any) => {
                if (response.id) {
                    this.token = response.token;

                    if (isRememberMe == true) {
                        localStorage.setItem('currentUser', JSON.stringify(response));
                        localStorage.setItem('accessToken', this.token);
                    }
                    
                    this.currentUser = response;
                    this.currentUser$.next(this.currentUser);
                }
                return response;
            }).catch(this.handleError);
    }

    private handleError(error: HttpErrorResponse) {
      return throwError((error.error).error || 'Server error');
    }

    isLogIn() {
      return this.currentUser != null;
    }

    refresh() {
      if (this.token == null) { return throwError("refresh(): token is null"); }

      return this.http.post(`${this.apiEndPoint}refresh-token`, "")
        .map((response: any) => {
            if (response.id) {
                this.token = response.token;
                this.currentUser.expiredAt = response.expiredAt;
                this.currentUser$.next(this.currentUser);
            }
        })
        .catch(this.handleError);
    }

    valid() {
        return this.http.get(`${this.apiEndPoint}validate-token`).catch(this.handleError);
    }

    logout() {
        localStorage.removeItem('currentUser');
        localStorage.removeItem('accessToken');
        this.currentUser = null;
        this.currentUser$.next(null);
    }
}
