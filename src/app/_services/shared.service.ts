import { Injectable, Inject } from '@angular/core';
import { Subject, Observable } from 'rxjs';

export class SharedService {
    themeToggle$: Subject<boolean>;

    constructor() {
        this.themeToggle$ = new Subject();
    }

    setThemeToggle(val: boolean) { this.themeToggle$.next(val); }
}