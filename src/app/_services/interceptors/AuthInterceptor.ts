import { throwError as observableThrowError,  Observable } from 'rxjs';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
import { Injectable, Inject, Injector } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpHeaders, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthenticationService } from '../core/authentication.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  private router: Router;
  private authService: AuthenticationService;

    constructor(
      private injector: Injector
    ) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    this.router = this.injector.get(Router);
    this.authService = this.injector.get(AuthenticationService);

    let token = this.authService.getToken();

    if (token != null) {
      request = request.clone({
          setHeaders: {
            'X-User-Token': `${token}`
          }
      });
    }

    return next.handle(request).do((event: HttpEvent<any>) => {
      if (event instanceof HttpResponse) {
          
      }
    }, (err: any) => {
      if (err instanceof HttpErrorResponse) {
        if (err.status === 403) {
          this.router.navigate(['./auth/logout']);
        }
      }
    });
  }
}
