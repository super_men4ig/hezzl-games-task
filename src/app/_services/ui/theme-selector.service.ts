import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { Injectable, Inject } from '@angular/core';
import { ThemeSet, DEFAULT_THEME_SET } from '../../_models/themeSet';

@Injectable()
export class ThemeSelectorService {
    private KEY_THEME_SET = 'themeSet'; 
    
    private _theme: ThemeSet;
    public get theme() : ThemeSet {
        return JSON.parse(localStorage.getItem(this.KEY_THEME_SET));
    }
    public set theme(v : ThemeSet) {
        console.info(v);
        this._theme = v;
        localStorage.setItem(this.KEY_THEME_SET, JSON.stringify(this._theme));
        this.theme$.next(this._theme);
    }
    public theme$: BehaviorSubject<any>;

    constructor() {
        if (this.theme == null) { this.theme = DEFAULT_THEME_SET; }
        this.theme$ = new BehaviorSubject(this.theme);
    }

    onChangeThemeSet(name, value) {
        this.theme[name] = value;
    }

    getThemeSets() {
        return Observable.of([
            {
                key: 'headercolor',
                type: 'color',
                values: ['default', 'primary', 'info', 'success', 'danger', 'dark']
            },
            {
                key: 'sidebarcolor',
                type: 'color',
                values: ['default', 'dark']
            },
            {
                key: 'rtl',
                type: 'toggle',
                values: [true, false]
            }
        ]);
    }
}
