import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthenticationService } from 'src/app/_services/core/authentication.service';

@Injectable()
export class AuthGuard implements CanActivate {
    constructor(private router: Router,
                private _authService: AuthenticationService) { }

    canActivate(route: ActivatedRouteSnapshot,
                state: RouterStateSnapshot) {

        // let currentUser: any = localStorage.getItem('currentUser');
        let currentUser: any = this._authService.currentUser;
        let hasAccess = false;
        if (currentUser) {
            // currentUser = JSON.parse(currentUser);

            if (currentUser.token != null) {
                hasAccess = true;

                if (hasAccess) {
                    return true;
                } else {
                    console.error('Access denied!');
                    return false;
                }
            }
        }

        let urlToRedirect = state.url.split('?')[0];
        console.info(state.url);

        let paramsToRedirect = { returnUrl: urlToRedirect };
        paramsToRedirect = Object.assign({}, paramsToRedirect, state.root.queryParams);
        this.router.navigate(['/auth/login'], { queryParams: paramsToRedirect });
        return false;
    }
}
