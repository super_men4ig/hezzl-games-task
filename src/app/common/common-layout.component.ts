import { Component, OnInit } from '@angular/core';
import { ThemeSelectorService } from 'src/app/_services/ui/theme-selector.service';
import { ThemeSet } from 'src/app/_models/themeSet';

@Component({
    selector: 'app-dashboard',
    templateUrl: './common-layout.component.html'
})

export class CommonLayoutComponent implements OnInit {

    public app : any;
    public changeHeader: any;
    public changeSidenav: any;
    public headerSelected: any;
    public sidenavSelected : any;
    public searchActived : any;
    public searchModel: any;

    constructor(private _themeSelectorService: ThemeSelectorService) {
        this.app = {
            layout: {
                sidePanelOpen: false,
                isMenuOpened: true,
                isMenuCollapsed: false,
                themeConfigOpen: false,
                rtlActived: false,
                searchActived: false
            }
        };  

        this.changeHeader = changeHeader;
    
        function changeHeader(headerTheme) {
            this.headerSelected = headerTheme;
        }

        this.changeSidenav = changeSidenav;
    
        function changeSidenav(sidenavTheme) {
            this.sidenavSelected = sidenavTheme;
        }

        this._themeSelectorService.theme$.subscribe((theme: ThemeSet) => {
            this.headerSelected = `header-${theme.headercolor}`;
            this.sidenavSelected = `side-nav-${theme.sidebarcolor}`;
            this.app.layout.rtlActived = theme.rtl;
        });
    }


    ngOnInit(){
    }
}
