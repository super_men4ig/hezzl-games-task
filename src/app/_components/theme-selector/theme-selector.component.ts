import { Component, OnInit } from '@angular/core';
import { SharedService } from '../../_services/shared.service';
import { ThemeSelectorService } from 'src/app/_services/ui/theme-selector.service';
import { Observable } from 'rxjs/internal/Observable';
import { BehaviorSubject } from 'rxjs';
import { ThemeSet } from '../../_models/themeSet';
import { AuthenticationService } from 'src/app/_services/core/authentication.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
    selector: 'app-theme-selector',
    templateUrl: './theme-selector.component.html'
})
export class ThemeSelectorComponent implements OnInit {
    public isOpen = false;

    public themeSet: Array<any> = [];
    public theme: ThemeSet;

    constructor(private sharedService: SharedService,
                private themeSelectorService: ThemeSelectorService,
                private _authService: AuthenticationService,
                private _route: ActivatedRoute,
                private _router: Router) 
    {
        this.sharedService.themeToggle$.subscribe((val) => { this.isOpen = val; });
        this.themeSelectorService.getThemeSets().subscribe((arr) => { this.themeSet = arr; });
        this.theme = this.themeSelectorService.theme;
    }

    ngOnInit():any {
    }

    toggleThemeSelector(val) {
        this.sharedService.setThemeToggle(val);
    }

    onSelectionChange(key, value) {
        console.info(key); console.info(value);
        this.theme[key] = value;
        this.themeSelectorService.theme = this.theme;
    }

    onClickTestLogout() {
        this._authService.logout();
        this.sharedService.setThemeToggle(false);
        this._router.navigate(['/auth/login']);

    }
}