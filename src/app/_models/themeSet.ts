export class ThemeSet {
    headercolor: string;
    sidebarcolor: string;
    rtl: boolean;

    constructor(headercolor, sidebarcolor, rtl) {
        this.headercolor = headercolor;
        this.sidebarcolor = sidebarcolor;
        this.rtl = rtl;
    }
}

export const DEFAULT_THEME_SET = new ThemeSet("default", "default", false);